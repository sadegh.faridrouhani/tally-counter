package sbu.cs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long result =0;
        for(int i=0;i<arr.length;i+=2){
            result+=arr[i];
        }
        return result;

    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int movement;
        int size=arr.length;
        for(int i=0;i<size/2;i++){
            movement =arr[i];
            arr[i]=arr[size-i-1];
            arr[size-i-1]= movement;
        }
        return arr;


    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        return null;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> L = new ArrayList<>();
        for (String[] name : names) {
            List<String> sublist = new ArrayList<>(Arrays.asList(name));
            L.add(sublist);
        }
        return L;

    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {

        List<Integer> list= new ArrayList<>();
        int k =2;
        while (n>1) {
            if (n% k == 0 ) {
                while (n% k ==0) {
                    n/= k;
                }
                list.add(k);
            }
            k++;
        }
        return list;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        String[] split=line.split("\\W+");
        return new ArrayList<>(Arrays.asList(split));

    }
}
