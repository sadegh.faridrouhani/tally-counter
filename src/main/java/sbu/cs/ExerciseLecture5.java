package sbu.cs;


public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
        StringBuilder result= new StringBuilder();
        for(int i=1;i<=length;i++){
            int randomNumber=(int)Math.floor(Math.random()*26)+97;
            result.append((char) randomNumber);
        }
        return result.toString();

    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        return null;
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
        int k = 1;

        do {
            if (fibo(k) + Integer.toBinaryString(fibo(k)).replace("0","").length() == n)
                return true;
            if (fibo(k) + Integer.toBinaryString(fibo(k)).replace("0","").length() > n)
                return false;
            k++;
        }while(true);
    }

    public int fibo (int i) {
        if (i == 0)
            return 0;
        if (i == 1)
            return 1;
        return fibo(i - 1) + fibo(i - 2);
    }

}



